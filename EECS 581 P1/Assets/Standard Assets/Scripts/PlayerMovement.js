﻿#pragma strict

function Start ()
{

}

var playerSpeed : float;

function Update ()
{
	transform.Translate(Input.GetAxisRaw("Horizontal") * Time.deltaTime * playerSpeed, 0, Input.GetAxisRaw("Vertical") * Time.deltaTime * playerSpeed);
	
	transform.position.x = Mathf.Clamp(transform.position.x, -3.6 , 3.6);
	transform.position.z = Mathf.Clamp(transform.position.z, -2.5 , 2.7);
}