﻿var enemies : GameObject[];
var player : GameObject;
private var spawnGrid;

private var level : int;
static var totalEnemies : int;
static var lives : int = 3;
static var gameOver : boolean;
static var score : int;
static var isWin : boolean;

var powerUpPrefab : GameObject;
static var powerUp : GameObject;

var guiSkin : GUISkin;

function Awake() {
	powerUp = powerUpPrefab;
	totalEnemies = 0;
	gameOver = false;
	score = 0;
	lives = 3;
	isWin = false;
	
	spawnGrid = new Array();
	spawnGrid[0] = new Array(Vector3(-2.7,1,3.0), Vector3(-2.7,1,2.3), Vector3(-2.7,1,1.6), Vector3(-2.7,1,0.9) );
	spawnGrid[1] = new Array(Vector3(-1.7,1,3.0), Vector3(-1.7,1,2.3), Vector3(-1.7,1,1.6), Vector3(-1.7,1,0.9) );
	spawnGrid[2] = new Array(Vector3(-0.7,1,3.0), Vector3(-0.7,1,2.3), Vector3(-0.7,1,1.6), Vector3(-0.7,1,0.9) );
	spawnGrid[3] = new Array(Vector3(0.3,1,3.0), Vector3(0.3,1,2.3), Vector3(0.3,1,1.6), Vector3(0.3,1,0.9) );
	spawnGrid[4] = new Array(Vector3(1.3,1,3.0), Vector3(1.3,1,2.3), Vector3(1.3,1,1.6), Vector3(1.3,1,0.9) );
	spawnGrid[5] = new Array(Vector3(2.3,1,3.0), Vector3(2.3,1,2.3), Vector3(2.3,1,1.6), Vector3(2.3,1,0.9) );

	Instantiate(player, Vector3(0,1,-2), transform.rotation);
}

function Update () {
	Debug.Log(totalEnemies);
	if (totalEnemies == 0) { 
		LevelEnd();
	}	
}

function LevelEnd() {
	level += 1;

	
	if (level == 1) {
		totalEnemies = 4;
		yield WaitForSeconds(1);
		Instantiate(enemies[0], spawnGrid[1][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[2][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[3][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[4][0], transform.rotation);
	} else if (level == 2) {
		totalEnemies = 6;
		yield WaitForSeconds(1);
		Instantiate(enemies[0], spawnGrid[1][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[2][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[3][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[4][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[2][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[3][1], transform.rotation);
	} else if (level == 3) {
		totalEnemies = 10;
		yield WaitForSeconds(1);
		Instantiate(enemies[0], spawnGrid[1][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[2][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[3][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[4][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[1][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[2][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[3][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[4][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][0], transform.rotation);
	} else if (level == 4) {
		totalEnemies = 14;
		yield WaitForSeconds(1);
		Instantiate(enemies[0], spawnGrid[1][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[2][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[3][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[4][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[1][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[2][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[3][1], transform.rotation);
		Instantiate(enemies[1], spawnGrid[4][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][1], transform.rotation);
		Instantiate(enemies[2], spawnGrid[2][2], transform.rotation);
		Instantiate(enemies[2], spawnGrid[3][2], transform.rotation);

	} else if (level == 5) {
		totalEnemies = 20;
		yield WaitForSeconds(1);
		Instantiate(enemies[2], spawnGrid[1][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[2][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[3][0], transform.rotation);
		Instantiate(enemies[2], spawnGrid[4][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[1][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[2][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[3][1], transform.rotation);
		Instantiate(enemies[1], spawnGrid[4][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][1], transform.rotation);
		Instantiate(enemies[2], spawnGrid[2][2], transform.rotation);
		Instantiate(enemies[2], spawnGrid[3][2], transform.rotation);
		Instantiate(enemies[1], spawnGrid[1][2], transform.rotation);
		Instantiate(enemies[1], spawnGrid[0][2], transform.rotation);
		Instantiate(enemies[1], spawnGrid[4][2], transform.rotation);
		Instantiate(enemies[1], spawnGrid[5][2], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][3], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][3], transform.rotation);

	} else if (level == 6) {
		totalEnemies = 24;
		yield WaitForSeconds(1);
		Instantiate(enemies[2], spawnGrid[1][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[2][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[3][0], transform.rotation);
		Instantiate(enemies[2], spawnGrid[4][0], transform.rotation);
		Instantiate(enemies[1], spawnGrid[1][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[2][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[3][1], transform.rotation);
		Instantiate(enemies[1], spawnGrid[4][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][0], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][1], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][1], transform.rotation);
		Instantiate(enemies[2], spawnGrid[2][2], transform.rotation);
		Instantiate(enemies[2], spawnGrid[3][2], transform.rotation);
		Instantiate(enemies[1], spawnGrid[1][2], transform.rotation);
		Instantiate(enemies[1], spawnGrid[0][2], transform.rotation);
		Instantiate(enemies[1], spawnGrid[4][2], transform.rotation);
		Instantiate(enemies[1], spawnGrid[5][2], transform.rotation);
		Instantiate(enemies[0], spawnGrid[0][3], transform.rotation);
		Instantiate(enemies[0], spawnGrid[5][3], transform.rotation);
		Instantiate(enemies[0], spawnGrid[1][3], transform.rotation);
		Instantiate(enemies[1], spawnGrid[2][3], transform.rotation);
		Instantiate(enemies[1], spawnGrid[3][3], transform.rotation);
		Instantiate(enemies[0], spawnGrid[4][3], transform.rotation);


	} else if (level == 7) { 
		isWin = true;
		gameOver = true;
	}

}

function Respawn() {
	yield WaitForSeconds(1.0);
	Instantiate(player, Vector3(0,1,-2.9), transform.rotation);
}

function OnGUI() {
	GUI.skin = guiSkin;
	GUI.Label(Rect(Screen.width / 2 - 285, 0, 400, 100), score.ToString() );
	
	if (gameOver) {
		if (GUI.Button(Rect(Screen.width / 2 - 140, 200, 300, 100), "Restart", "menubutton") ) {
			Application.LoadLevel("Game");
		}
	}
	
	if (isWin) { 
		GUI.Label(Rect(Screen.width / 2 - 140, 100, 300, 100), "You Win!");
	}
}