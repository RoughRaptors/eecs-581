﻿#pragma strict

var explosion : GameObject;
var explosionSound : GameObject;

function Start ()
{

}

function Update ()
{

}

function OnTriggerEnter(col : Collider)
{
	if(col.gameObject.tag == "blueBullet")
	{
		Instantiate(explosion, transform.position, transform.rotation);
		Instantiate(explosionSound, transform.position, transform.rotation);
		
		var enemies : GameController = gameObject.GetComponent(GameController);
		enemies.totalEnemies -= 1;
	
		Destroy(col.gameObject);
		Destroy(gameObject);
	}
}