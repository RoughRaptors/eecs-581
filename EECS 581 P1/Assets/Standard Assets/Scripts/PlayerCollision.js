﻿#pragma strict

var player : GameObject;

var explosion : GameObject;
var explosionSound : GameObject;

function Start ()
{

}

function Update () 
{
	
}


function OnTriggerEnter(col : Collider)
{
	if(col.gameObject.tag == "enemyRedBullet")
	{
		Instantiate(explosion, transform.position, transform.rotation);
		Instantiate(explosionSound, transform.position, transform.rotation);

		Instantiate(player, transform.position, transform.rotation);
		
		var lives : GameController = gameObject.GetComponent(GameController);
		lives.lives -= 1;
		//GameController.lives -= 1;
		//gameObject.Find("GameController").GetComponent(GameController).Respawn();
		
		Destroy(col.gameObject);
		Destroy(gameObject);
	}
}
