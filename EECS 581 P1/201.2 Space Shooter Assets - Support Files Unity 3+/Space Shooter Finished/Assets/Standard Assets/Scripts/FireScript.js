var laserObj : GameObject[];
var fireSound : GameObject;
private var laserType : int;
private var laserTimer : float;

var maxFireFreq : float;
private var lastShot : float;

function Update () {
	if (Input.GetButtonDown("Fire1") && (Time.time > lastShot + maxFireFreq) ) {
		Fire();
	}
	
	laserTimer -= 1 * Time.deltaTime;
	if (laserTimer < 0) {
		laserType = 0;
	}
}

function Fire() {
	lastShot = Time.time;
	if (laserType == 0) {
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[0], transform.position, transform.rotation);
	} else if (laserType == 1) {
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[1], transform.position, Quaternion.Euler(Vector3(0, -20, 0)) );
		yield WaitForSeconds(0.1);
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[1], transform.position, transform.rotation);
		yield WaitForSeconds(0.1);
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[1], transform.position, Quaternion.Euler(Vector3(0, 20, 0)) );
	} else if (laserType == 2) {
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[1], transform.position, Quaternion.Euler(Vector3(0, -30, 0)) );
		yield WaitForSeconds(0.08);
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[1], transform.position, Quaternion.Euler(Vector3(0, -15, 0)) );
		yield WaitForSeconds(0.08);
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[1], transform.position, transform.rotation);
		yield WaitForSeconds(0.08);
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[1], transform.position, Quaternion.Euler(Vector3(0, 15, 0)) );
		yield WaitForSeconds(0.08);
		Instantiate(fireSound, transform.position, transform.rotation);
		Instantiate(laserObj[1], transform.position, Quaternion.Euler(Vector3(0, 30, 0)) );

	}
	
}

function PowerUpLaser() {
	laserType += 1;
	laserType = Mathf.Clamp(laserType,0,2);
	laserTimer = 3;
}