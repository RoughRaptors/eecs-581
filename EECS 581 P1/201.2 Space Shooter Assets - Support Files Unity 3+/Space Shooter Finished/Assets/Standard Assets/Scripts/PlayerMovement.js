var playerSpeed : float;
var engineSound : GameObject;

function Update () {		 
	
	
	transform.Translate(Input.GetAxisRaw("Horizontal") * Time.deltaTime * playerSpeed, 0, Input.GetAxisRaw("Vertical") * Time.deltaTime * playerSpeed);
	
	transform.position.x = Mathf.Clamp(transform.position.x, -3.4, 3.4);
	transform.position.z = Mathf.Clamp(transform.position.z, -3.1, 3.1);
}