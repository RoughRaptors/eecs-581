DontDestroyOnLoad(this);

var gameName = "Change this game name";
var serverPort = 25002;

function OnGUI() {
	if (Network.peerType == NetworkPeerType.Disconnected) {
	
		if (GUILayout.Button ("Start Server")) {
			Network.InitializeServer(32, serverPort);
			MasterServer.RegisterHost(gameName, "stuff", "l33t game for all");
		}
		
		if (GUILayout.Button ("Refresh available Servers")  ) {
			MasterServer.RequestHostList (gameName);
		}
		
		var data : HostData[] = MasterServer.PollHostList();
		for (var element in data)
		{

				var name = element.gameName + " " + element.connectedPlayers + " / " + element.playerLimit;
				GUILayout.Label(name);	
				var hostInfo;
				hostInfo = "[";
				// Here we display all IP addresses, there can be multiple in cases where
				// internal LAN connections are being attempted. In the GUI we could just display
				// the first one in order not confuse the end user, but internally Unity will
				// do a connection check on all IP addresses in the element.ip list, and connect to the
				// first valid one.
				for (var host in element.ip)
				{
					hostInfo = hostInfo + host + ":" + element.port + " ";
				}
				hostInfo = hostInfo + "]";
				//GUILayout.Label("[" + element.ip + ":" + element.port + "]");	
				GUILayout.Label(hostInfo);	
				GUILayout.Label(element.comment);
				if (GUILayout.Button("Connect"))
				{
					Network.Connect(element.ip, element.port);			
				}

		}
	}
	else
	{
		if (GUILayout.Button ("Disconnect"))
		{
			Network.Disconnect();
			MasterServer.UnregisterHost();
		}
	}
		
}