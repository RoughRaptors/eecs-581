var points : int;
var pointsGui : GameObject;
var explosion : GameObject;
var explosionSound : GameObject;
var health : int;

function Update () {
}

function OnTriggerEnter (col : Collider) {
	if (col.gameObject.tag == "Laser") {
		health -= 1;
		Instantiate(explosionSound, transform.position, transform.rotation);
		Instantiate(explosion, transform.position, transform.rotation);
		Destroy(col.gameObject);
		if (health < 1) {
			Drop();
			GameController.totalEnemies -= 1;
			GameController.score += points;
			var viewportPos : Vector3 = Camera.main.WorldToViewportPoint(transform.position);
			Instantiate(pointsGui, Vector3(viewportPos.x, viewportPos.y, 0), transform.rotation);
			Destroy(gameObject);
		}
	}
}

function Drop() {
	if (Random.Range(0,20) < 3) {
		Instantiate(GameController.powerUp, transform.position, transform.rotation);
	}
}