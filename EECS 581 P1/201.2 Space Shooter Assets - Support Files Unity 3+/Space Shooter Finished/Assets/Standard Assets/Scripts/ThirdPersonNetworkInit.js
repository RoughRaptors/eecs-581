function OnNetworkInstantiate (msg : NetworkMessageInfo) {
	// This is our own player
	if (networkView.isMine)
	{
//		Camera.main.SendMessage("SetTarget", transform);
//		GetComponent("NetworkInterpolatedTransform").enabled = false;
	}
	// This is just some remote controlled player
	else
	{
		name += "Remote";
		GetComponent(PlayerMovement).enabled = false;
		GetComponent(PlayerCollision).enabled = false;
		GetComponent(FireScript).enabled = false;
//		GetComponent("NetworkInterpolatedTransform").enabled = true;
	}
}
