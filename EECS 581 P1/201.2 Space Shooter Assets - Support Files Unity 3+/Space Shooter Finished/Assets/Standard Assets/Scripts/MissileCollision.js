var explosion : GameObject;
var explosionSound : GameObject;

function OnTriggerEnter (col : Collider) {
	if (col.gameObject.tag == "Laser") {
			Instantiate(explosion, transform.position, transform.rotation);
			Instantiate(explosionSound, transform.position, transform.rotation);
			Destroy(col.gameObject);
			Destroy(gameObject);
	}
}