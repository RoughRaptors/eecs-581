var pickupSound : GameObject;

function OnTriggerEnter (col : Collider) {
	if (col.gameObject.tag == "Player") {
		Instantiate(pickupSound, transform.position, transform.rotation);
		GameObject.Find("Player(Clone)").GetComponent(FireScript).PowerUpLaser();
		Destroy(gameObject);
	}
}