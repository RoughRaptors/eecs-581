private var spawnTime : float;
var invinceDelay : float;
var explosion : GameObject;
var explosionSound : GameObject;

function Awake () {
	spawnTime = Time.time;
}

function OnTriggerEnter (col : Collider) {
	if ( ( (col.gameObject.tag == "EnemyProjectile") || (col.gameObject.tag == "Enemy") ) && (Time.time > spawnTime + invinceDelay) ) {
			Instantiate(explosion, transform.position, transform.rotation);
			Instantiate(explosionSound, transform.position, transform.rotation);
			GameController.lives -= 1;
			if (GameController.lives > 0) {
				gameObject.Find("GameController").GetComponent(GameController).Respawn();
			} else {
				GameController.gameOver = true;
			}
			if (col.gameObject.tag == "Enemy") {
				GameController.totalEnemies -= 1;
			}
			Destroy(col.gameObject);
			Destroy(gameObject);
	}
}