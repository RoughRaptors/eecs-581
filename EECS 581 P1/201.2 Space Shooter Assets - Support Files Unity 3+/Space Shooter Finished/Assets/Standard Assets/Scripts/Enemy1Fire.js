var fireFreq : float;
var projectile : GameObject;
var fireSound : GameObject;

private var lastShot : float;

function Start () {
	fireFreq *= Random.Range(0.8,1.2);
	lastShot = Time.time + Random.Range(-fireFreq,0);
}

function Update () {
	if (Time.time > lastShot + fireFreq) {
		Fire();
	}
}

function Fire() {
	lastShot = Time.time;
	Instantiate(projectile, transform.position, transform.rotation);
	Instantiate(fireSound, transform.position, transform.rotation);

}