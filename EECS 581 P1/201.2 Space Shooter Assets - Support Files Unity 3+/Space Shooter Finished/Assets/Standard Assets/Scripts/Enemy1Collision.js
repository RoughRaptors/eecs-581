var points : int;
var pointsGui : GameObject;
var explosion : GameObject;
var explosionSound : GameObject;

function Update () {
}

function OnTriggerEnter (col : Collider) {
	if (col.gameObject.tag == "Laser") {
		Drop();
		GameController.totalEnemies -= 1;
		GameController.score += points;
		var viewportPos : Vector3 = Camera.main.WorldToViewportPoint(transform.position);
		Instantiate(pointsGui, Vector3(viewportPos.x, viewportPos.y, 0), transform.rotation);
		Instantiate(explosion, transform.position, transform.rotation);
		Instantiate(explosionSound, transform.position, transform.rotation);
		Destroy(col.gameObject);
		Destroy(gameObject);
	}
}

function Drop() {
	if (Random.Range(0,20) < 3) {
		Instantiate(GameController.powerUp, transform.position, transform.rotation);
	}
}